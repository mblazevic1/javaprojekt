package com.example.javaprojekt;


import java.io.Serializable;
import java.util.Random;


public class Customer implements Serializable {
    public String getID() {
        return ID;
    }

    private String ID;
    private String name;
    private String username;
    private String password;
    private double cash;

    public Customer(String name, String username, String password) {
        this.ID = RandomGenerator.GenerateID();
        this.cash = RandomGenerator.GenerateCash();
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public double getCash() {
        return cash;
    }

    public void removeCash(double cash) {
        this.cash -= cash;
    }

}
