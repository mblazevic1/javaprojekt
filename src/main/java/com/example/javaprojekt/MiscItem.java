package com.example.javaprojekt;

public class MiscItem extends Item {
    private String ID;
    private int quantity;
    private double cash;

    public MiscItem(String name, double price, int quantity) {
        super(name, price);
        ID = RandomGenerator.GenerateID();
        this.quantity = quantity;
        this.cash = RandomGenerator.GenerateCash();
    }
    public String getQuantity() {
        return quantity + " pcs";
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
