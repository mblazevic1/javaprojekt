package com.example.javaprojekt;

public class Food extends Item {
    private String ID;
    private int quantity;

    public Food(String name, Double price, int quantity){
        super(name,price);
        ID = RandomGenerator.GenerateID();
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity + " kg";
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


}
