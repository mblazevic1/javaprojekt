package com.example.javaprojekt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Store implements SwitchScenes{

    @FXML
    private TableColumn<Item, String> nameCol;
    @FXML
    private TableColumn<Item, Double> priceCol;
    @FXML
    private TableColumn<Item, Object> quantityCol;
    @FXML
    private TableColumn<Item, String> nameCol1;
    @FXML
    private TableColumn<Item, Double> priceCol1;
    @FXML
    private TableColumn<Item, Object> quantityCol1;
    @FXML
    private Label lblCash;

    @FXML
    private TableView<Item> tvItems;

    @FXML
    private TableView<Item> tvCart;

    @FXML
    private TextField tfFilter;

    private ObservableList<Item> items = FXCollections.observableArrayList();
    private ObservableList<Item> cartItems = FXCollections.observableArrayList();
    private ItemsDB itemsDB;
    private LoggedUser loggedUser = LoggedUser.getInstance();

    public Store() {
        itemsDB = new ItemsDB();
        loadData();
    }

    private void loadData() {
        items.setAll(itemsDB.getItems());

    }
    private void updateCashLabel() {
        double cash = loggedUser.getCash();
        lblCash.setText(String.format("Cash: $%.2f", cash));
    }

    @FXML
    public void initialize() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        nameCol1.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceCol1.setCellValueFactory(new PropertyValueFactory<>("price"));
        quantityCol1.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        tvItems.setItems(items);
        tvCart.setItems(cartItems);
        updateCashLabel();
        tfFilter.textProperty().addListener((observable, oldValue, newValue) -> Filter(newValue));
    }

    @FXML
    void Filter(String value) {
        ObservableList<Item> sortedItems = items.stream()
                .filter(item -> item.getName().toLowerCase().contains(value.toLowerCase()))
                .collect(Collectors.toCollection(FXCollections::observableArrayList));

        tvItems.setItems(sortedItems);

    }
    @FXML
    private void btnAdd(ActionEvent event) throws IOException {
        Item selectedItem = tvItems.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            cartItems.add(selectedItem);
            items.remove(selectedItem);
            itemsDB.deleteItem(selectedItem);
            tvItems.refresh();
            tvCart.refresh();
        }
    }
    @FXML
    private void btnReturn(ActionEvent event){
        Item selectedItem = tvCart.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            items.add(selectedItem);
            cartItems.remove(selectedItem);
            itemsDB.addItem(selectedItem);
            tvItems.refresh();
            tvCart.refresh();
        }
    }

    public void SwitchToScene(ActionEvent event) throws IOException {
        List<Item> CheckoutItems = new ArrayList<>(cartItems);
        FXMLLoader fxmlLoader = new FXMLLoader(LoginApplication.class.getResource("checkout.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 750, 650);
        Checkout checkout = fxmlLoader.getController();
        checkout.setCartItems(CheckoutItems);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("DUCAN");
        stage.setScene(scene);
        stage.show();
    }





}
