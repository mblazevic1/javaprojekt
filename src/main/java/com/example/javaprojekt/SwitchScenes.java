package com.example.javaprojekt;


import javafx.event.ActionEvent;
import java.io.IOException;

public interface SwitchScenes {
    public void SwitchToScene(ActionEvent event) throws IOException;
}