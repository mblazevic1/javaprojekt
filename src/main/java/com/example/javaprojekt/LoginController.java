package com.example.javaprojekt;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class LoginController implements SwitchScenes {


    @FXML
    private TextField tfUsername;

    @FXML
    private TextField tfPassword;
    @FXML
    private TextField tfName;

    private CustomerDB customer = new CustomerDB();



    public void SwitchToRegister(ActionEvent event) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(LoginApplication.class.getResource("register.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 750, 650);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("SIGNUP");
        stage.setScene(scene);
        stage.show();
    }

    public void Register() throws IOException {
        String username = tfUsername.getText();
        String password = tfPassword.getText();
        String name = tfName.getText();
        customer.addCustomer(name,username,password);
        Node sourceNode = (Node) tfUsername;
        Stage stage = (Stage) sourceNode.getScene().getWindow();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 380);
        stage.setScene(scene);

    }


    public void SwitchToScene(ActionEvent event) throws IOException {
        String username = tfUsername.getText();
        String password = tfPassword.getText();
        if(!tfUsername.getText().contains("admin") && !tfPassword.getText().contains("admin")) {
            if (customer.auth(username, password) != null) {
                LoggedUser loggedUser = LoggedUser.getInstance();
                String ID = customer.auth(username,password);
                loggedUser.login(ID);
                FXMLLoader fxmlLoader = new FXMLLoader(LoginApplication.class.getResource("store.fxml"));
                Scene scene = new Scene(fxmlLoader.load(), 750, 650);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setTitle("DUCAN");
                stage.setScene(scene);
                stage.show();
            } else {
                tfUsername.setText("ERROR");
            }
        }else{
            FXMLLoader fxmlLoader = new FXMLLoader(LoginApplication.class.getResource("inventoryManager.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 750, 650);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setTitle("DUCAN ADMIN");
            stage.setScene(scene);
            stage.show();

        }
    }
}