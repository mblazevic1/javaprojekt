package com.example.javaprojekt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;

public class InventoryManager {
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfPrice;
    @FXML
    private TextField tfQuantity;
    @FXML
    private TextField tfQuantityAmount;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnDelete;
    @FXML
    private TableView tvInventory;
    @FXML
    private TableColumn<Item, String> nameCol;
    @FXML
    private TableColumn<Item, Double> priceCol;
    @FXML
    private TableColumn<Item, Object> quantityCol;
    @FXML
    private RadioButton rbFood;
    @FXML
    private RadioButton rbDrink;
    @FXML
    private RadioButton rbMisc;
    private ObservableList<Item> items = FXCollections.observableArrayList();
    private ItemsDB itemsDB = new ItemsDB();
    private ToggleGroup itemTypeGroup = new ToggleGroup();

    public InventoryManager() {
        loadData();
    }

    private void loadData() {
        items.setAll(itemsDB.getItems());
    }

    @FXML
    public void initialize() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        tvInventory.setItems(items);


        rbDrink.setToggleGroup(itemTypeGroup);
        rbFood.setToggleGroup(itemTypeGroup);
        rbMisc.setToggleGroup(itemTypeGroup);


        itemTypeGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                oldValue.setSelected(true);
            }
        });

    }

    public void btnDelete() throws IOException {
        Item selectedItem = (Item) tvInventory.getSelectionModel().getSelectedItem();
        System.out.println(selectedItem);
        if (selectedItem != null) {
            items.remove(selectedItem);
            itemsDB.deleteItem(selectedItem);
            loadData();
        }
    }
    public void btnAdd(){
        String name = tfName.getText();
        Double price = Double.parseDouble(tfPrice.getText());
        if (rbDrink.isSelected()) {
            double quantity = Double.parseDouble(tfQuantity.getText());
            itemsDB.addItem(new Drink(name,price,quantity));
            loadData();
        } else if (rbFood.isSelected()) {
            int quantity = Integer.parseInt(tfQuantity.getText());
            itemsDB.addItem(new Food(name,price,quantity));
            loadData();
        } else if (rbMisc.isSelected()) {
            int quantity = Integer.parseInt(tfQuantity.getText());
            itemsDB.addItem(new MiscItem(name,price,quantity));
            loadData();
        }
    }
}






