package com.example.javaprojekt;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class CustomerDB {
    private List<Customer> customers;


    public CustomerDB() {
        customers = new ArrayList<>();
        try {
            loadFromFile("customers.dat");

        } catch (IOException | ClassNotFoundException e) {
            customers.add(new Customer("name", "", ""));
            customers.add(new Customer("ADMIN", "admin", "admin"));
            try {
                FileHandler.saveToFile(customers,"customers.dat");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public void addCustomer(String name, String username, String password) {
        customers.add(new Customer(name, username, password));
        try {
            FileHandler.saveToFile(customers, "customers.dat");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String auth(String username, String password) {
        for (Customer customer : customers) {
            if (customer.getUsername().equals(username) && customer.getPassword().equals(password)) {
                return customer.getID();
            }
        }
        return null;
    }

    public double getUserCash(String ID){
        for (Customer customer : customers) {
            if (customer.getID().equals(ID) ) {
                return customer.getCash();
            }
        }
        return 0.0;
    }
    public void removeUserCash(String ID, double cash) throws IOException {
        for (Customer customer : customers) {
            if (customer.getID().equals(ID)) {
                customer.removeCash(cash);
                FileHandler.saveToFile(customers,"customers.dat");
                return;
            }
        }
    }

    private void loadFromFile(String filename) throws IOException, ClassNotFoundException {
        Object obj = FileHandler.loadFromFile(filename);
        if (obj instanceof List<?>) {
            customers = (List<Customer>) obj;
        } else {
            throw new IOException("There was error");
        }
    }
}