package com.example.javaprojekt;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Checkout {
    @FXML
    private TableView<Item> tvCheckout;
    @FXML
    private TableColumn<Item, String> nameCol;
    @FXML
    private TableColumn<Item, Double> priceCol;
    @FXML
    private TableColumn<Item, Object> quantityCol;
    @FXML
    private Label lblCash;
    @FXML
    private Label lblTotal;
    private ObservableList<Item> cartItems = FXCollections.observableArrayList();
    private LoggedUser loggedUser = LoggedUser.getInstance();
    private ItemsDB itemsDB = new ItemsDB();

    public Checkout(){

    }

    public void initialize() {
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        quantityCol.setCellValueFactory(new PropertyValueFactory<>("quantity"));

        tvCheckout.setItems(cartItems);

    }

    private void refreshLabels(){
        updateCashLabel();
        updateTotalLabel();
    }

    private void updateCashLabel() {
        double cash = getCash();
        lblCash.setText(String.format("Cash: $%.2f", cash));
    }
    private double getCash(){
        return loggedUser.getCash();
    }

    private void updateTotalLabel(){
        double total = getTotal();
        lblTotal.setText(String.format("Total: $%.2f", total));

    }
    private double getTotal(){
        double total = 0.0;
        for (Item item : cartItems) {
            total += item.getPriceValue();
        }
        return total;
    }
    public void setCartItems(List<Item> cartItems) {
        this.cartItems.setAll(cartItems);
        refreshLabels();
    }

    public void btnDelete(ActionEvent event){
        Item selectedItem = tvCheckout.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            cartItems.remove(selectedItem);
            tvCheckout.refresh();
            updateTotalLabel();
        }
    }

    public void btnCheckout(ActionEvent event) throws IOException {
        if(loggedUser.getCash() >= getTotal()){
            loggedUser.removeCash(getTotal());
            cartItems.clear();
            tvCheckout.refresh();
            updateCashLabel();
            updateTotalLabel();
        }else{
            lblTotal.setText("INSUFFICIENT FUNDS!");
        }

    }




}
