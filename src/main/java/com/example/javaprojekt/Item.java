package com.example.javaprojekt;

import java.io.Serializable;

public abstract class Item implements Serializable {
    private String name;
    private double price;
    private String quantity;

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getPrice() {
        return price + "€";
    }
    public double getPriceValue() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}
