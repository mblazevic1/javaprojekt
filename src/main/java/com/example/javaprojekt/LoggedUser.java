package com.example.javaprojekt;

import java.io.IOException;

public class LoggedUser{
    private static LoggedUser instance = null;
    private String UserId;
    private CustomerDB customerDB = new CustomerDB();

     public LoggedUser() {
        UserId = null;
    }

    public static LoggedUser getInstance() {
        if (instance == null) {
            instance = new LoggedUser();
        }
        return instance;
    }

    public void login(String userId) {
        UserId = userId;
    }
    public double getCash(){
        return customerDB.getUserCash(UserId);
    }
    public void removeCash(double total) throws IOException {
        String ID = getUserId();
        customerDB.removeUserCash(ID,total);
    }

    public String getUserId() {
        return UserId;
    }

    public void logout() {
        UserId = null;
    }
    public void saveToFile(String filename){

    }
}
