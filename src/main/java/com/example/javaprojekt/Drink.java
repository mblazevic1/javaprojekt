package com.example.javaprojekt;

public class Drink extends Item {
    private String ID;
    private double quantity;


    public Drink(String name, double price, double quantity) {
        super(name, price);
        ID = RandomGenerator.GenerateID();
        this.quantity = quantity;
    }

    public String getQuantity() {
        return quantity + " L";
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }


}