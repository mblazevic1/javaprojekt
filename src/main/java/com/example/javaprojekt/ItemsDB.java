package com.example.javaprojekt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ItemsDB {
    private List<Item> items;

    public ItemsDB() {
        items = new ArrayList<>();
        try {
            loadFromFile("items.dat");
        } catch (IOException | ClassNotFoundException e) {
            DefaultItems();
        }

    }

    private void DefaultItems() {
        items.add(new Food("Chicken Breast", 5.49, 1));
        items.add(new Drink("Pepsi", 1.75, 1.5));
        items.add(new MiscItem("Headphones", 59.99, 1));
        items.add(new Food("Salmon Fillet", 8.99, 1));
        items.add(new Drink("Orange Juice", 2.25, 1));
        items.add(new MiscItem("Smartwatch", 199.99, 1));
        items.add(new Food("Pasta", 3.99, 1));
        items.add(new Drink("Iced Tea", 1.50, 1));
        items.add(new MiscItem("Backpack", 39.99, 1));
        items.add(new Food("Cheeseburger", 6.49, 1));
        items.add(new Drink("Lemonade", 2.25, 1));
        items.add(new MiscItem("External Hard Drive", 79.99, 1));
        items.add(new Food("Sushi Roll", 9.99, 1));
        items.add(new Drink("Coffee", 2.75, 1));
        items.add(new MiscItem("Bluetooth Speaker", 49.99, 1));
        try {
            FileHandler.saveToFile(items, "items.dat");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

   public void deleteItem(Item itemToDelete) throws IOException{
        items.remove(itemToDelete);
        FileHandler.saveToFile(items, "items.dat");
    }

    public void addItem(Item itemToAdd) {
       items.add(itemToAdd);
       try {
           FileHandler.saveToFile(items,"items.dat");
       } catch (IOException e) {
           e.printStackTrace();
       }
   }

    public List<Item> getItems() {
        return items;
    }

    private void loadFromFile(String filename) throws IOException, ClassNotFoundException {
        Object obj = FileHandler.loadFromFile(filename);
        if (obj instanceof List<?>) {
            items = (List<Item>) obj;
        } else {
            throw new IOException("There was error while loading the file");
        }
    }
}
